

# database = {pred: [[pattern], [preds], [pattern], [preds]], pr1: [...]}

# like: $db | declare("mortal", ["m"], [["man", "m"]])
#           | declare("man", ["_aristotle"], [])
#           | query(["mortal", "_aristotle"])

def issymbol: (type | inside(["array", "object"])) or isvariable | not ;
# compare def symbol($str): $str | split("") | .[0] | . == "_"
def isvariable: try startswith("_") catch false ;

def declare($pred; $pattern; $body):
  .[$pred] |= (. // [","]) + [$body];

def unify($a; $b):
  if $a == $b then
    .
  elif ($a|issymbol) and ($b | isvariable) then
    unify($b, $a)
  elif $a|isvariable then
    .[$a] = $b
  elif $a|type == "array" then
    if $b|type == "array" then
      unify($a|.[0], $b|.[0])
      |unify($a|.[1:], $b|.[1:])
    elif $b|isvariable then
      unify($b; $a)
    else
      empty
    end
  else
    empty
  end
;

def call($expr):
  ($expr | .[0]) as $name
  |($expr|.[1:]) as $args
  |if $name == "," then
    _comma ;
    

issymbol


